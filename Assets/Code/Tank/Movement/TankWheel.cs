using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace TestGame
{
    public class TankWheel : MonoBehaviour
    {
        [SerializeField] private float couplingRadius;
        [SerializeField] private Transform connectedTrack;

        [SerializeField] private float velocityMultiplier = 5;

        private List<TrackPart> trackParts;
        private Collider wheelColider;

        private void Awake()
        {
            trackParts = new List<TrackPart>(64);
            connectedTrack.GetComponentsInChildren(true, trackParts);
            wheelColider = GetComponent<Collider>();
            wheelColider.material = wheelColider.material;
        }

        public void SetTorque(float torque)
        {
            if (torque == 0)
            {
                wheelColider.material.staticFriction = 10;
                wheelColider.material.dynamicFriction = 8;
                wheelColider.material.frictionCombine = PhysicMaterialCombine.Maximum;
            }
            else
            {
                wheelColider.material.staticFriction = 0;
                wheelColider.material.dynamicFriction = 0;
                wheelColider.material.frictionCombine = PhysicMaterialCombine.Minimum;
            }

            List<TrackPart> nearParts = new List<TrackPart>(16);
            var sqrRadius = couplingRadius * couplingRadius;
            foreach (var trackPart in trackParts)
                if (Vector3.SqrMagnitude(trackPart.PhysPosition - transform.position) < sqrRadius)
                    nearParts.Add(trackPart);

            foreach (var trackPart in nearParts)
            {
                trackPart.Mark = true;
                trackPart.SetVelocity(torque * velocityMultiplier / nearParts.Count);
            }
        }

        private void OnDrawGizmosSelected()
        {
            var c = Color.red;
            c.a = 0.5F;
            Gizmos.color = c;
            Gizmos.DrawSphere(transform.position, couplingRadius);
        }

        private void OnValidate()
        {
            if (couplingRadius < 0.01F)
                couplingRadius = 0.01F;
        }
    }
}
