using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace TestGame
{
    [ExecuteAlways]
    public class TrackPart : MonoBehaviour
    {
        private HingeJoint joint;
        private Rigidbody partRB;

        [SerializeField] private float force;
        [SerializeField] private float anchorNodeIdent;

        public bool Mark { get; set; }

        public Vector3 PhysPosition => GetComponent<Rigidbody>().position;

        private void Awake()
        {
            joint = GetComponent<HingeJoint>();
            partRB = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            joint.axis = transform.right;
            joint.autoConfigureConnectedAnchor = false;
            joint.connectedAnchor = Vector3.back * anchorNodeIdent;
            joint.anchor = Vector3.forward * anchorNodeIdent;
        }

        public void Setup(TrackPart next)
        {
            joint.connectedBody = next.partRB;
        }

        public void SetVelocity(float velocityMagnitude)
        {
            var velocityDir = velocityMagnitude * transform.forward;
            partRB.AddForce(velocityDir, ForceMode.VelocityChange);
        }

        private void OnDrawGizmos()
        {
            if (!Mark)
                return;

            var c = Color.red;
            c.a = 0.3F;
            Gizmos.color = c;
            Gizmos.DrawCube(partRB.position, GetComponentInChildren<MeshRenderer>().transform.localScale);

            Mark = false;
        }
    }
}
